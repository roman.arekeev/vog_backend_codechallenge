using System;
using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.DynamoDBEvents;
using Amazon.Lambda.TestUtilities;
using VogCodeChallenge.AWS;
using Xunit;

namespace VogCodeChallenge.Tests
{
    public class AwsFunctionTests
    {
        [Fact]
        public void TestFunction()
        {
            var evnt = new DynamoDBEvent
            {
               Records = new List<DynamoDBEvent.DynamodbStreamRecord>
               {
                   new DynamoDBEvent.DynamodbStreamRecord
                   {
                       AwsRegion = "us-west-2",
                       EventName = OperationType.MODIFY,
                       Dynamodb = new StreamRecord
                       {
                           ApproximateCreationDateTime = DateTime.Now,
                           Keys = new Dictionary<string, AttributeValue> { {"id", new AttributeValue { S = "12343535" } } },
                           NewImage = new Dictionary<string, AttributeValue> { { "field1", new AttributeValue { S = "NewValue" } }, { "field2", new AttributeValue { S = "AnotherNewValue" } } },
                           OldImage = new Dictionary<string, AttributeValue> { { "field1", new AttributeValue { S = "OldValue" } }, { "field2", new AttributeValue { S = "AnotherOldValue" } } },
                           StreamViewType = StreamViewType.NEW_AND_OLD_IMAGES
                       }
                   }
               }
            };

            var context = new TestLambdaContext();
            var function = new DynamoDbEntryEventHandler();

            function.Handle(evnt, context);

            var testLogger = context.Logger as TestLambdaLogger;
            Assert.Contains("Updated entries", testLogger?.Buffer.ToString());
        }  
    }
}