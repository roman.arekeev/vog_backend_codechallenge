using System;
using System.Collections.Generic;
using System.Linq;
using VogCodeChallenge.Tests.Exceptions;
using VogCodeChallenge.Utils;
using Xunit;

namespace VogCodeChallenge.Tests
{
    public class QuestionClassTests
    {
        [Fact]
        public void GetNames_Returns_NotEmptyValues()
        {
            var enumerator = QuestionClass.Names.GetEnumerator();

            var names = new List<string>();

            while (enumerator.MoveNext())
            {
                var name = enumerator.Current;
                names.Add(name);
            }
            
            Assert.NotEmpty(names);
            Assert.Equal(QuestionClass.NamesCount, names.Count);
            Assert.True(names.All(n => !string.IsNullOrWhiteSpace(n)));
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4 })]
        public void TestModule_Int1to4_ReturnsIntMultiplyBy2(int[] values)
        {
            foreach (var value in values)
            {
                var res = QuestionClass.TestModule(value);
                
                Assert.Equal(value * 2, res);
            }
        }
        
        [Theory]
        [InlineData(new int[] { 5, 10, 789, 100000 })]
        public void TestModule_IntGreater4_ReturnsIntMultiplyBy3(int[] values)
        {
            foreach (var value in values)
            {
                var res = QuestionClass.TestModule(value);
                
                Assert.Equal(value * 3, res);
            }
        }
        
        [Theory]
        [InlineData(new int[] { 0, -1, -500 })]
        public void TestModule_IntLess1_TrowsException(int[] values)
        {
            foreach (var value in values)
            {
                Assert.Throws<InappropriateValueException>(() => QuestionClass.TestModule(value));
            }
        }
        
        [Theory]
        [InlineData(new float[] { 1.0f, 2.0f })]
        public void TestModule_Float1and2_ReturnsFloat3(float[] values)
        {
            foreach (var value in values)
            {
                var res = QuestionClass.TestModule(value);
                
                Assert.Equal( 3.0f, res);
            }
        }
        
        [Theory]
        [InlineData("one")]
        [InlineData("two")]
        [InlineData("")]
        [InlineData(null)]
        public void TestModule_String_ReturnsUpperCase(string? value)
        {
            var res = QuestionClass.TestModule(value);
            string resStr = res as string;

            if (string.IsNullOrWhiteSpace(value))
            {
                Assert.True(string.IsNullOrWhiteSpace(resStr));
            }
            else
            {
                Assert.True(StringComparer.Ordinal.Equals(value.ToUpperInvariant(), resStr));
            }
        }
        
        [Theory]
        [MemberData(nameof(GetArbitraryData))]
        public void TestModule_AnyOtherValue_ReturnsValue(object value, object expectedValue)
        {
            var res = QuestionClass.TestModule(value);
                
            Assert.Equal(expectedValue, res);
        }

        public static IEnumerable<object[]> GetArbitraryData()
        {
            yield return new object[] { 234.56m, 234.56m };
            yield return new object[] { DateTime.Today, DateTime.Today };
        }
    }
}
