## How to run the API service locally (runs in Development env)

Run the command:
```
    dotnet run --project src/VogCodeChallenge.API/VogCodeChallenge.API.csproj
```

## How to run the API service in Docker container (runs in Production env)

```
    docker-compose up -d
```

## Get all employees
Paste the url to Postman, browser, etc. and make the request  
http://localhost:5000/employees/

## Get concrete department employees
Paste the url to Postman, browser, etc. and make the request  
http://localhost:5000/employees/department/{departmentId}

**departmentId** can be found in result of request http://localhost:5000/employees/

## Get custom error message
Paste the url to Postman, browser, etc. and make the request
http://localhost:5000/errortest/

## Execute unit tests
```
    dotnet test
```
