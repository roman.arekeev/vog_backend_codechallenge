using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VogCodeChallenge.API.DataStore;
using VogCodeChallenge.API.GlobalFilters;
using VogCodeChallenge.API.Repositories;
using VogCodeChallenge.API.Services;

namespace VogCodeChallenge.API
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        
        public Startup(IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (_hostEnvironment.IsDevelopment())
            { 
                services.AddSingleton<IEmployeeRepository, EmployeeInMemoryRepository>();
            }
            else
            {
                services.AddSingleton<IEmployeeRepository, EmployeeDataStoreRepository>();   
            }

            services.AddSingleton<EmployeesDataStore>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            
            services.AddHostedService<HostedServices.DataOutputHostedService>();

            services.AddControllers(
                opt =>
                {
                    opt.Filters.Add<GlobalExceptionFilter>();
                });
        }

        public void Configure(IApplicationBuilder app)
        {
            if (_hostEnvironment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
