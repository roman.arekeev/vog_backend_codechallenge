namespace VogCodeChallenge.API.GlobalFilters
{
    public class JsonErrorResponse
    {
        public string Message { get; set; }

        public string? DeveloperMessage { get; set; }
    }
}
