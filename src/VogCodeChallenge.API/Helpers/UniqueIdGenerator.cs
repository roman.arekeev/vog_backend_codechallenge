﻿using System;

namespace VogCodeChallenge.API.Helpers
{
    public static class UniqueIdGenerator
    {
        public static Guid NewId => Guid.NewGuid();
    }
}