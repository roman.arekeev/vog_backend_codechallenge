using System;
using Microsoft.AspNetCore.Mvc;

namespace VogCodeChallenge.API.Controllers
{
    [Route("[controller]")]
    public class ErrorTest : ControllerBase
    {
        [HttpGet]
        public IActionResult CheckError() => throw new ArgumentException("some test exception");
    }
}