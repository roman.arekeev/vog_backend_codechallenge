﻿using System;
using Microsoft.AspNetCore.Mvc;
using VogCodeChallenge.API.Services;

namespace VogCodeChallenge.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public IActionResult GetAll() =>
            Ok(_employeeService.ListAll());

        [HttpGet("department/{departmentId}")]
        public IActionResult GetByDepartment(Guid departmentId) =>
            Ok(_employeeService.GetByDepartment(departmentId));
    }
}