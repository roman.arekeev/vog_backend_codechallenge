﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VogCodeChallenge.API.Repositories;

namespace VogCodeChallenge.API.HostedServices
{
    public class DataOutputHostedService : IHostedService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<DataOutputHostedService> _logger;

        public DataOutputHostedService(IEmployeeRepository employeeRepository, ILogger<DataOutputHostedService> logger)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var allEmployees = _employeeRepository
                .GetAll()
                .GroupBy(e => e.DepartmentId)
                .ToDictionary(g => g.Key);

            foreach (var (departmentId, employees) in allEmployees)
            {
                var employeeIds = string.Join(',', employees.Select(e => e.Id));
                _logger.LogInformation("Department: {0}, Employees: {1}", departmentId, string.Join(',', employeeIds));
            }

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken) =>
            Task.CompletedTask;
    }
}