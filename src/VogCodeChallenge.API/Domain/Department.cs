﻿using System;
using System.Collections.Generic;

namespace VogCodeChallenge.API.Domain
{
    public class Department
    {
        private readonly List<Employee> _employees = new List<Employee>();

        public Department(string name, string address)
        {
            Id = Helpers.UniqueIdGenerator.NewId;
            Name = IsNameValid(name) ? name : throw new ArgumentException(nameof(name));
            Address = IsAddressValid(address) ? address : throw new ArgumentException(nameof(address));
        }

        public Guid Id { get; }

        public string Name { get; }

        public string Address { get; }

        public IReadOnlyCollection<Employee> Employees => _employees;

        public void AddEmployee(Employee employee)
        {
            if (employee is null)
            {
                throw new ArgumentNullException(nameof(employee));
            }

            if (employee.DepartmentId != Guid.Empty)
            {
                throw new ArgumentException("The employee belongs to another department and can not be added to current department");
            }

            employee.SetDepartmentId(Id);

            _employees.Add(employee);
        }

        private static bool IsNameValid(string name) => !string.IsNullOrWhiteSpace(name);

        private static bool IsAddressValid(string address) => !string.IsNullOrWhiteSpace(address);
    }
}