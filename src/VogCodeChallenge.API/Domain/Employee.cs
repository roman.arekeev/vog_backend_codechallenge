﻿using System;

namespace VogCodeChallenge.API.Domain
{
    public class Employee
    {
        public Employee(string firstName, string lastName, string jobTitle, string address)
        {
            Id = Helpers.UniqueIdGenerator.NewId;

            FirstName = IsFirstNameValid(firstName) ? firstName : throw new ArgumentException(nameof(firstName));
            LastName = IsLastNameValid(lastName) ? lastName : throw new ArgumentException(nameof(lastName));
            JobTitle = IsJobTitleValid(jobTitle) ? jobTitle : throw new ArgumentException(nameof(jobTitle));
            Address = IsAddressValid(address) ? address : throw new ArgumentException(nameof(address));
        }

        public Guid Id { get; }

        public Guid DepartmentId { get; private set; }

        public string FirstName { get; }

        public string LastName { get; }

        public string JobTitle { get; }

        public string Address { get; }

        public void SetDepartmentId(Guid departmentId) =>
            DepartmentId = IsDepartmentIdValid(departmentId) ? departmentId : throw new ArgumentException(nameof(departmentId));

        private bool IsFirstNameValid(string firstName) => !string.IsNullOrWhiteSpace(firstName);

        private bool IsLastNameValid(string lastName) => !string.IsNullOrWhiteSpace(lastName);

        private bool IsJobTitleValid(string jobTitle) => !string.IsNullOrWhiteSpace(jobTitle);

        private bool IsAddressValid(string address) => !string.IsNullOrWhiteSpace(address);

        private bool IsDepartmentIdValid(Guid departmentId) => departmentId != Guid.Empty;
    }
}