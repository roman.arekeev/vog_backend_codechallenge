using System.Collections.Generic;
using VogCodeChallenge.API.Domain;

namespace VogCodeChallenge.API.DataStore
{
    public class EmployeesDataStore
    {
        private readonly List<Department> departments;

        public EmployeesDataStore()
        {
            departments = GetDepartmentsData();
        }

        public IReadOnlyCollection<Department> Departments => departments;

        private List<Department> GetDepartmentsData()
        {
            var department1 = new Department("Department 3", "3630 Patterson Road, Brooklyn");
            department1.AddEmployee(new Employee("Lucas", "McGrath", "Developer", "590 Hilltop Street, Springfield"));
            department1.AddEmployee(new Employee("Jake", "Forsyth", "Developer", "4575 Friendship Lane, Springfield"));
            department1.AddEmployee(new Employee("Justin", "Gibson", "Tester", "822 Maple Avenue, Boise"));

            var department2 = new Department("Department 4", "1102 Woodside Circle, Fort Walton Beach");
            department2.AddEmployee(new Employee("Simon", "Short", "Manager", "4036 Star Trek Drive, Tallahassee"));
            department2.AddEmployee(new Employee("William", "Knox", "Tester", "4094 Beechwood Drive, Bloomfield"));
            department2.AddEmployee(new Employee("Adam", "Dowd", "Developer", "2659 Jehovah Drive, Rocky Mount"));

            return new List<Department> { department1, department2 };
        }
    }
}