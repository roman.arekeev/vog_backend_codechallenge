﻿using System;
using System.Collections.Generic;
using System.Linq;
using VogCodeChallenge.API.Domain;
using VogCodeChallenge.API.Repositories;

namespace VogCodeChallenge.API.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _repository;

        public EmployeeService(IEmployeeRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Employee> GetAll() =>
            _repository.GetAll();

        public IList<Employee> ListAll() =>
            _repository.GetAll().ToList();

        public IEnumerable<Employee> GetByDepartment(Guid departmentId) =>
            _repository.GetDepartmentEmployees(departmentId);
    }
}