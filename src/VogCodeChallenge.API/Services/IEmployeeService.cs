﻿using System;
using System.Collections.Generic;
using VogCodeChallenge.API.Domain;

namespace VogCodeChallenge.API.Services
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetAll();

        IList<Employee> ListAll();

        IEnumerable<Employee> GetByDepartment(Guid departmentId);
    }
}