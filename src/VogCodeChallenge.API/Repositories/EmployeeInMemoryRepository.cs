﻿using System;
using System.Collections.Generic;
using System.Linq;
using VogCodeChallenge.API.Domain;

namespace VogCodeChallenge.API.Repositories
{
    public class EmployeeInMemoryRepository : IEmployeeRepository
    {
        private static readonly List<Department> departments;

        static EmployeeInMemoryRepository()
        {
            var department1 = new Department("Department 1", "3630 Patterson Road, Brooklyn");
            department1.AddEmployee(new Employee("David", "Burgess", "Manager", "590 Hilltop Street, Springfield"));
            department1.AddEmployee(new Employee("Dominic", "Campbell", "Developer", "4575 Friendship Lane, Springfield"));
            department1.AddEmployee(new Employee("Edward", "Chapman", "Tester", "822 Maple Avenue, Boise"));

            var department2 = new Department("Department 2", "1102 Woodside Circle, Fort Walton Beach");
            department2.AddEmployee(new Employee("Simon", "Short", "Manager", "4036 Star Trek Drive, Tallahassee"));
            department2.AddEmployee(new Employee("William", "Knox", "Tester", "4094 Beechwood Drive, Bloomfield"));
            department2.AddEmployee(new Employee("Adam", "Dowd", "Developer", "2659 Jehovah Drive, Rocky Mount"));

            departments = new List<Department> { department1, department2 };
        }

        public IEnumerable<Employee> GetAll() => departments.SelectMany(d => d.Employees);

        public IReadOnlyCollection<Employee> GetDepartmentEmployees(Guid departmentId) =>
            departments.FirstOrDefault(d => d.Id == departmentId)?.Employees ?? Array.Empty<Employee>();
    }
}