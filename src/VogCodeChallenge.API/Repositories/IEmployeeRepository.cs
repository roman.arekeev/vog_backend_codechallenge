﻿using System;
using System.Collections.Generic;
using VogCodeChallenge.API.Domain;

namespace VogCodeChallenge.API.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        IReadOnlyCollection<Employee> GetDepartmentEmployees(Guid departmentId);
    }
}