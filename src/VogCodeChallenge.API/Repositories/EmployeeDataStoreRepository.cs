using System;
using System.Collections.Generic;
using System.Linq;
using VogCodeChallenge.API.DataStore;
using VogCodeChallenge.API.Domain;

namespace VogCodeChallenge.API.Repositories
{
    public class EmployeeDataStoreRepository : IEmployeeRepository
    {
        private readonly EmployeesDataStore _dataStore;

        public EmployeeDataStoreRepository(EmployeesDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public IEnumerable<Employee> GetAll() => 
            _dataStore.Departments.SelectMany(d => d.Employees);

        public IReadOnlyCollection<Employee> GetDepartmentEmployees(Guid departmentId) =>
            _dataStore.Departments.FirstOrDefault(d => d.Id == departmentId)?.Employees ?? Array.Empty<Employee>();
    }
}