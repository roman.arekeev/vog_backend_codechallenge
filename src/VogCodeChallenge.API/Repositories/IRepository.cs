﻿using System.Collections.Generic;

namespace VogCodeChallenge.API.Repositories
{
    public interface IRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();
    }
}