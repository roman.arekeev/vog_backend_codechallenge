using System;

namespace VogCodeChallenge.Tests.Exceptions
{
    public class InappropriateValueException : Exception
    {
        public InappropriateValueException()
        {
        }

        public InappropriateValueException(string message) : base(message)
        {
        }
    }
}