﻿using System;
using System.Collections.Generic;
using VogCodeChallenge.Tests.Exceptions;

namespace VogCodeChallenge.Utils
{
    public class QuestionClass
    {
        static List<string> NamesList = new List<string>
        {
            "Jimmy",
            "Jeffrey",
            "John",
        };

        public static List<string> Names => NamesList;

        public static int NamesCount => NamesList.Count;

        public static object TestModule(object? obj) => obj switch
        {
            int i when i > 0 && i < 5 => i * 2,
            int j when j > 4 => j * 3,
            int k when k < 1 => throw new InappropriateValueException($"{k} is less than 1"),
            float f when Math.Abs(f - 1.0f) < 0.0001 || Math.Abs(f - 2.0f) < 0.0001 => 3.0f,
            string s when !string.IsNullOrWhiteSpace(s) => s.ToUpperInvariant(),
            _ => obj
        };
    }
}