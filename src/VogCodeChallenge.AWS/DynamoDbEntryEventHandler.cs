using System.Collections.Generic;
using System.Linq;
using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace VogCodeChallenge.AWS
{
    public class DynamoDbEntryEventHandler
    {
        public void Handle(DynamoDBEvent dynamoEvent, ILambdaContext context)
        {
            var modifiedRecords = dynamoEvent.Records.Where(r => r.EventName == OperationType.MODIFY)
                .ToArray();

            if (modifiedRecords.Length > 0)
            {
                var ids = new List<string>(modifiedRecords.Length);
                
                foreach (var record in modifiedRecords)
                {
                    if (record.Dynamodb.Keys.TryGetValue("id", out var attr) && !string.IsNullOrWhiteSpace(attr.S))
                    {
                        ids.Add(attr.S);
                    }
                }

                if (ids.Count > 0)
                {
                    context.Logger.LogLine($"Updated entries: {string.Join(',', ids)}");  
                }
            }
        }
    }
}